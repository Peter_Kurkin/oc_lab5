#include <stdio.h>
#define MAX 15

/*
 * Написать программу вывода сообщения на экран.
 */

int main()
{
    char name[MAX];
    puts("Enter message:");
    fgets(name, MAX, stdin);
    printf("Your message: %s", name);
    return 0;
}
