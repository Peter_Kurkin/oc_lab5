#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/stat.h>

#define PATH_MAX 1000

/*
 * Написать программу вывода на экран содержимого текущего и заданного первым параметром
 * вызова программы каталогов. Предусмотреть контроль ошибок открытия/закрытия/чтения каталога.
 */

void print_dir_list(char *name_folder, DIR *folder) {
    struct dirent *entry;

    printf("%s\n", name_folder);
    entry = readdir(folder);
    while (entry) {
        printf("%s\n", entry->d_name);
        entry = readdir(folder);
    }
}


int main(int arg, char **args) {
    DIR *this_folder;
    DIR *folder_to;
    char *name_folder = args[1];

    this_folder = opendir(".");
    folder_to = opendir(name_folder);

    if (!this_folder || !folder_to) {
        printf("Не смог прочитать директорию");
        return 1;
    }

    print_dir_list("Текущая директория", this_folder);
    print_dir_list("Под директория", folder_to);


    return 0;
}
