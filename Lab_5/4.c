/*
 Написать программу вывода содержимого текстового файла на экран
 (в качестве аргумента при запуске программы передаётся имя файла, второй аргумент (N)
 устанавливает вывод по группам строк (по N строк) или сплошным текстом (N=0)).
 Для вывода очередной группы строк необходимо ожидать нажатия пользователем любой клавиши.
 Для чтения или записи файла использовать только функции посимвольного ввода-вывода getc(), putc(), fgetc(), fputc().
 Предусмотреть контроль ошибок открытия/закрытия/чтения/записи файла.
 */

#include <stdio.h>

#define MAX 10000

int main(int arg, char **args) {
    char recieve_text;
    int new_line_characters_count;
    char *name = args[1];
    int N = *args[2] - '0';

    FILE *fp = fopen(name, "r");

    // Return if could not open file
    if (fp == NULL) {
        fprintf(stderr, "%s\n", "Error in open file.");
        return -1;
    }

    if (N == 0) {
        recieve_text = fgetc(fp);
        while (recieve_text != EOF) {
            putc(recieve_text, stdout);
            recieve_text = fgetc(fp);
        }
    } else {

        while (1) {

            new_line_characters_count = 0;
            recieve_text = fgetc(fp);
            while (recieve_text != EOF) {
                putc(recieve_text, stdout);
                if (recieve_text == '\n' && (++new_line_characters_count) == N) {
                    break;
                }
                recieve_text = fgetc(fp);
            }
            if (recieve_text == EOF) {
                if (feof(fp) != 0) {
                    printf("Я всё прочитал\n");
                    return 0;
                } else {
                    printf("Ошибочка\n");
                    return 0;
                }
            }
            printf("Нажми что-нибудь и я продолжу \n");
            getchar();
        }

    }

    int check = fclose(fp);
    if (check) {
        puts("file cant be close");
        return 1;
    }

    return 0;
}