/*
Написать программу копирования одного файла в другой. В качестве параметров при вызове программы передаются
имена первого и второго файлов. Для чтения или записи файла использовать только функции посимвольного ввода-вывода
getc(), putc(), fgetc(), fputc(). Предусмотреть копирование прав доступа
к файлу и контроль ошибок открытия/закрытия/чтения/записи файла.
*/

#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>


//#define MAX 100

int main(int arg, char **args) {
    char *first_file_name = args[1];
    char *second_file_name = args[2];
    char ch;

    FILE *F1 = fopen(first_file_name, "r");
    FILE *F2 = fopen(second_file_name, "a");

    if (!F1) {
        puts("F1 cant be open");
        return 1;
    }

    if (!F2) {
        puts("F2 cant be open");
        return 1;
    }

    while ((ch = getc(F1)) != EOF) {
        fputc(ch, F2);
    }

    int f_check = fclose(F1);
    int s_check = fclose(F2);

    if (f_check) {
        puts("F1 cant be close");
        return 1;
    }

    if (s_check) {
        puts("F2 cant be close");
        return 1;
    }

    struct stat file1_stat;
    stat(first_file_name, &file1_stat);
    chmod(second_file_name, file1_stat.st_mode);


    return 0;
}
