/*
3. Написать программу ввода символов с клавиатуры и записи их в файл
    (в качестве аргумента при запуске программы вводится имя файла).
    Для чтения или записи файла использовать только функции посимвольного
    ввода-вывода getc(),putc(), fgetc(),fputc(). Предусмотреть выход после ввода
    определённого символа (например: ctrl-F). Предусмотреть контроль
    ошибок открытия/закрытия/чтения файла.
*/

#include <stdio.h>
#define MAX 1000000

int main(int argc, char **args)
{
    FILE *fd;
    fd=fopen(args[1],"w");


    if (fd==NULL) {
        fprintf(stderr,"%s\n","Error in open file.");
        return -1;
    }

    int c;
    while ((c=getc(stdin)) != EOF)
        fputc(c,fd);

    if (fclose(fd) != 0)
    {
        fprintf(stderr,"%s\n","Error in close file.");
    }
    return 0;
}