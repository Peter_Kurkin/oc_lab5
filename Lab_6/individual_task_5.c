#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdbool.h>

/*
 Разработать программу «интерпретатор команд», которая воспринимает команды,
 вводимые с клавиатуры, (например, ls -l /bin/bash)
 и осуществляет их корректное выполнение.
 Для этого каждая вводимая команда должна выполняться в отдельном процессе
 с использованием вызова exec(). Предусмотреть контроль ошибок.
 */

#define MAX_STRING 200
#define MAX_ARGS 100


void split(char *string, char **into_strings) {
    //split в питоне
    char *part = strtok(string, " \n");

    int i;
    for (i = 0; part != NULL; i++) {
        into_strings[i] = part;
        part = strtok(NULL, " \n");
    }
    into_strings[i] = NULL;
}

bool handle_command(char **argv) {
    pid_t pid = fork();
    if (pid == 0) {
        pid_t performing_pid = fork();
        if (performing_pid == 0) {
            execvp(argv[0], argv);
            printf("Не удалось найти команду: %s\n", argv[0]);
        } else if (performing_pid > 0) {
            int status;
            waitpid(performing_pid, &status, 0);
            if (WIFEXITED(status)) {
                int exit_status = WEXITSTATUS(status);
                if (exit_status != 0) {
                    printf("Команда завершилась, код: %d\n", exit_status);
                }
            } else {
                printf("Во время выполняния команды произошла неожиданная ошибка\n");
            }
        } else if (performing_pid < 0) {
            printf("Ошибка создания процесса для выполнения команды\n");
        }
        return false;
    } else if (pid < 0) {
        printf("Ошибка создания процесса\n");
    }
    return true;
}

int main() {
    bool aContinue = true;
    while (aContinue) {
        char command[MAX_STRING];
        fgets(command, MAX_STRING, stdin);

        char *argv[MAX_ARGS];
        split(command, argv);

        aContinue = handle_command(argv);
    }
    return 0;
}
